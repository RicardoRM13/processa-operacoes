package ricardorm13.operations.service;

import com.google.gson.Gson;
import ricardorm13.operations.entity.AccountEntity;
import ricardorm13.operations.entity.OperationEntity;

import java.util.ArrayList;
import java.util.List;

public class AccountService implements ProcessaOperacoes {

    @Override
    public Integer processaOperacoes(OperationEntity o, boolean acccountCreated, Integer firstTran, Integer limit) {
        Gson gson = new Gson();
        List<String> violations = new ArrayList<String>();
        AccountEntity accountEntity = new AccountEntity(limit, acccountCreated);

        if (!acccountCreated){
            accountEntity = gson.fromJson(o.getValue(), AccountEntity.class);
            limit = accountEntity.getAvaLimit();
        }else{
            violations.add("account-already-initialized");
        }

        String retorno = createReturn(violations, limit);

        System.out.println(retorno);

        return limit;
    }

    @Override
    public String createReturn(List<String> violations, Integer limit) {
        Output o = new Output(violations, limit);
        return o.createReturn();
    }

}
