package ricardorm13.operations.service;

import ricardorm13.operations.entity.OperationEntity;

import java.util.List;

public interface ProcessaOperacoes {

    public Integer processaOperacoes(OperationEntity o, boolean acccountCreated, Integer firstTran, Integer limit);

    public String createReturn(List<String> violations, Integer limit);
}
