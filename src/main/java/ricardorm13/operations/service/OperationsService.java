package ricardorm13.operations.service;

import ricardorm13.operations.entity.OperationEntity;

import java.util.List;

public class OperationsService {
    private ProcessaOperacoes po;

    public OperationsService(ProcessaOperacoes po){
        this.po = po;
    }

    public Integer processaOperacoes(OperationEntity o, boolean created, Integer firstTran, Integer limit){
        return this.po.processaOperacoes(o, created, firstTran, limit);
    }

}
