package ricardorm13.operations.entity;

import com.google.gson.annotations.SerializedName;

public class AccountEntity {

    @SerializedName("available-limit")
    private Integer avaLimit;

    public boolean isCreated() {
        return created;
    }

    public void setCreated(boolean created) {
        this.created = created;
    }

    private boolean created;

    public Integer getAvaLimit() {
        return avaLimit;
    }

    public void setAvaLimit(Integer avaLimit) {
        this.avaLimit = avaLimit;
    }

    public AccountEntity(Integer limit, boolean created){
        this.avaLimit = limit;
        this.created = created;
    }
}
