package ricardorm13.operations.service;

import java.util.List;

public class Output {

    private List<String> violations;
    private Integer limit;

    public Output(List<String> violations, Integer limit){
        this.violations = violations;
        this.limit = limit;
    }

    public String createReturn() {
        return "{\"account\": {\"available-limit\":" + this.limit + "}, \"violations\": " + this.violations + "}";
    }
}
