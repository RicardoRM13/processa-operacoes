package ricardorm13.operations;

import org.junit.Assert;
import org.junit.Test;
import ricardorm13.operations.entity.OperationEntity;
import ricardorm13.operations.service.AccountService;
import ricardorm13.operations.service.OperationsService;

public class AccountServiceTest {

    AccountService as = new AccountService();

    @Test
    public void accountServiceContaJaExisteTest(){

        OperationEntity oe = new OperationEntity("account", "{\"available-limit\":200}");

        Integer limit = 0;

        limit = as.processaOperacoes(oe,false,0, limit);

        OperationEntity oe2 = new OperationEntity("account", "{\"available-limit\":100}");

        Integer novoLimite = 0;

        novoLimite = as.processaOperacoes(oe2,true,0, limit);

        Assert.assertEquals(limit,novoLimite);
    }

    @Test
    public void accountServiceNovaContaTest(){

        OperationEntity oe = new OperationEntity("account", "{\"available-limit\":200}");

        Integer limit = 0;

        limit = as.processaOperacoes(oe,false,0, limit);

        Assert.assertEquals(limit, new Integer(200));
    }

}
