package ricardorm13.operations.service;

import com.google.gson.Gson;
import ricardorm13.operations.entity.OperationEntity;
import ricardorm13.operations.entity.TransactionEntity;

import java.util.ArrayList;
import java.util.List;

public class TransactionService implements ProcessaOperacoes {

    @Override
    public Integer processaOperacoes(OperationEntity o, boolean acccountCreated, Integer firstTran, Integer limit) {
        Gson gson = new Gson();
        List<String> violations = new ArrayList<String>();
        TransactionEntity t = gson.fromJson(o.getValue(), TransactionEntity.class);

        if (!acccountCreated){
            violations.add("account-not-initialized");
        }
        if (limit * 0.9 < t.getValue() && firstTran == 0){
            violations.add("first-transaction-above-threshold");
        }
        if (limit - t.getValue() < 0){
            violations.add("insufficient-limit");
        }
        if (violations.size() == 0) {
            limit = limit - t.getValue();
        }
        String retorno = createReturn(violations, limit);

        System.out.println(retorno);

        return limit;
    }

    @Override
    public String createReturn(List<String> violations, Integer limit) {
        Output o = new Output(violations, limit);
        return o.createReturn();
    }

}
