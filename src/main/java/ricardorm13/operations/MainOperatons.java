package ricardorm13.operations;

import ricardorm13.operations.entity.OperationEntity;
import ricardorm13.operations.service.AccountService;
import ricardorm13.operations.service.OperationsService;
import ricardorm13.operations.service.ProcessaOperacoes;
import ricardorm13.operations.service.TransactionService;

import java.rmi.server.Operation;
import java.util.ArrayList;
import java.util.List;

public class MainOperatons {

    public static void main (String[] args){
        try {
            List<OperationEntity> listTran = new ArrayList<OperationEntity>();

            //listTran.add(new OperationEntity("transaction", "{\"merchant\":\"Burger King\", \"amount\":80}"));
            listTran.add(new OperationEntity("account", "{\"available-limit\":100}"));
            listTran.add(new OperationEntity("transaction", "{\"merchant\":\"Burger King\", \"amount\":91}"));
            listTran.add(new OperationEntity("transaction", "{\"merchant\":\"Habibs\", \"amount\":91}"));
            listTran.add(new OperationEntity("transaction", "{\"merchant\":\"McDonalds\", \"amount\":3}"));
            listTran.add(new OperationEntity("transaction", "{\"merchant\":\"McDonalds\", \"amount\":95}"));
            listTran.add(new OperationEntity("transaction", "{\"merchant\":\"McDonalds\", \"amount\":76}"));
            listTran.add(new OperationEntity("transaction", "{\"merchant\":\"McDonalds\", \"amount\":75}"));
            listTran.add(new OperationEntity("account", "{\"available-limit\":200}"));


            boolean created = false;
            Integer cont = 0;
            Integer limitAnt = 0;
            Integer limit = 0;


            for (OperationEntity o: listTran) {

                if (o.getOperation().equals("transaction")){
                    ProcessaOperacoes pot = new TransactionService();
                    OperationsService os = new OperationsService(pot);
                    limit = os.processaOperacoes(o, created, cont, limit);
                    if (limit != limitAnt){
                        cont ++;
                        limitAnt = limit;
                    }

                }
                if (o.getOperation().equals("account")) {
                    ProcessaOperacoes poo = new AccountService();
                    OperationsService os = new OperationsService(poo);
                    limit = os.processaOperacoes(o, created, cont, limit);
                    limitAnt = limit;
                    created = true;
                }

            }


        }catch (Exception e){
            System.out.println(e);
        }

    }
}
