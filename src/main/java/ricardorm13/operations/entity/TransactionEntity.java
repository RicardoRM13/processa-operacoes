package ricardorm13.operations.entity;

import com.google.gson.annotations.SerializedName;

public class TransactionEntity {
    @SerializedName("merchant")
    private String merchant;

    @SerializedName("amount")
    private Integer value;

    public String getMerchant() {
        return merchant;
    }

    public void setMerchant(String operation) {
        this.merchant = operation;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public TransactionEntity(String operation, Integer value){
        this.merchant = operation;
        this.value = value;
    }
}
